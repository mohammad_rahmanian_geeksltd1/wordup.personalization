﻿-- Categories Table ========================
CREATE TABLE Categories (
    Id int IDENTITY(1,1) PRIMARY KEY,
    Name nvarchar(200)  NOT NULL,
    [Group] nvarchar(200)  NULL,
    ImageLight_FileName nvarchar(1500)  NULL,
    ImageDark_FileName nvarchar(1500)  NULL
);

