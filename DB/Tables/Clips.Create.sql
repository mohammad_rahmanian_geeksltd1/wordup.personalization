﻿-- Clips Table ========================
CREATE TABLE Clips (
    Id int IDENTITY(1,1) PRIMARY KEY,
    Subject int  NOT NULL,
    Title nvarchar(200)  NULL,
    Url nvarchar(200)  NOT NULL,
    Channel nvarchar(200)  NULL,
    VideoId nvarchar(200)  NULL,
    Duration int  NULL,
    CoveredWords int  NULL
);
CREATE INDEX [IX_Clips->Subject] ON Clips (Subject);

GO

