﻿ALTER TABLE Clips ADD Constraint
                [FK_Clip.Subject]
                FOREIGN KEY (Subject)
                REFERENCES Subjects (ID)
                ON DELETE NO ACTION;
GO