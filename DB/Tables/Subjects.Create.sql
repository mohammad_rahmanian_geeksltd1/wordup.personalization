﻿-- Subjects Table ========================
CREATE TABLE Subjects (
    Id int IDENTITY(1,1) PRIMARY KEY,
    Category int  NOT NULL,
    Name nvarchar(200)  NOT NULL,
    Image_FileName nvarchar(1500)  NULL
);
CREATE INDEX [IX_Subjects->Category] ON Subjects (Category);

GO

