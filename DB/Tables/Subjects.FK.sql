﻿ALTER TABLE Subjects ADD Constraint
                [FK_Subject.Category]
                FOREIGN KEY (Category)
                REFERENCES Categories (ID)
                ON DELETE NO ACTION;
GO