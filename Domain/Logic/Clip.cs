namespace Domain
{
    using System;
    using System.ComponentModel;
    using System.Threading.Tasks;
    using Olive;
    using Olive.Entities;

    /// <summary>
    /// Provides the business logic for Clip class.
    /// </summary>
    partial class Clip
    {
        public override Task Validate()
        {
            var _videoId = Url.GetYouTubeVideoIdFromUrl();
            if (_videoId.IsEmpty())
                throw new ValidationException("The url you provided is not a valid Youtube url");
            return base.Validate();
        }
        protected override async Task OnSaving(CancelEventArgs e)
        {
            await base.OnSaving(e);
            var _videoId = Url.GetYouTubeVideoIdFromUrl();

            VideoId = _videoId;
        }
    }
}