﻿using MSharp;

namespace Domain
{
    public class Category : EntityType
    {
        public Category()
        {
           
            PrimaryKeyType("int").AssignIDByDatabase();
            String("Name").Mandatory();
            String("Group");
            SecureImage("ImageLight").ValidExtensions("png");
            SecureImage("ImageDark").ValidExtensions("png");
            InverseAssociate<Subject>("Subjects", "Category");
            Int("Number Of Subjects").Calculated().Getter("Subjects.Count().GetAwaiter().GetResult()");
            Int("Number Of Clips").Calculated().Getter("(int)Subjects.GetList().Sum(x => x.NumberOfClips ?? 0).GetAwaiter().GetResult()");
        }
    }
}