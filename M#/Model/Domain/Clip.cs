﻿using MSharp;

namespace Domain
{
    public class Clip : EntityType
    {
        public Clip()
        {
            PrimaryKeyType("int").AssignIDByDatabase();
            Associate<Subject>("Subject").Mandatory();
            String("Title");
            String("Url").Mandatory();
            String("Channel");
            String("VideoId");
            Int("Duration");
            Int("CoveredWords");
            
        }
    }
}