﻿using MSharp;

namespace Domain
{
    public class Subject : EntityType
    {
        public Subject()
        {
            PrimaryKeyType("int").AssignIDByDatabase();
            Associate<Category>("Category").Mandatory();
            String("Name").Mandatory();
            SecureImage("Image").ValidExtensions("jpg, jpeg");
            InverseAssociate<Clip>("Clips", "Subject");
            Int("Number Of Clips").Calculated().Getter("Clips.Count().GetAwaiter().GetResult()");
        }
    }
}