using Domain;
using MSharp;

namespace Modules
{
    public class Footer : GenericModule
    {
        const string DEVELOPER = "http://www.geeks.ltd.uk";

        public Footer()
        {
            VisibleIf(CommonCriterion.IsUserLoggedIn);

            var logout = Link("Logout")
                      .Icon(FA.PowerOff)
                      .ValidateAntiForgeryToken(false)
                      .OnClick(x =>
                      {
                          x.CSharp("await OAuth.Instance.LogOff();");
                          x.Go<LoginPage>();
                      });

            IsInUse().IsViewComponent()
                .Using("Olive.Security")
                .RootCssClass("website-footer")
                .Markup($@"<div class=""row"">
                                <div class=""column logout"">
                                    {logout.Ref}
                                </div>
                                <div class=""column footer-text"">
                                     [#BUTTONS(SoftwareDevelopment)#] by [#BUTTONS(Geeks)#] &copy; @LocalTime.Now.Year. All rights reserved.
                                 </div>
                            </div>");

            Link("Geeks")
                .OnClick(x => x.Go(DEVELOPER, OpenIn.NewBrowserWindow));

            Link("Software development")
                .CssClass("plain-text")
                .OnClick(x => x.Go(DEVELOPER, OpenIn.NewBrowserWindow));
        }
    }
}