using MSharp;
using Domain;

namespace Modules
{
    public class AdminSettingsMenu : MenuModule
    {
        public AdminSettingsMenu()
        {
            SubItemBehaviour(MenuSubItemBehaviour.ExpandCollapse);
            WrapInForm(false);
            AjaxRedirect();
            IsViewComponent();
            RootCssClass("sidebar-menu");
            UlCssClass("nav flex-column");
            Using("Olive.Security");

            Item("Categories")
                .OnClick(x => x.Go<CategoryPage>());

            Item("Subjects")
               .OnClick(x => x.Go<SubjectPage>());

            Item("Administrators")
                .OnClick(x => x.Go<Admin.Settings.AdministratorsPage>());

            Item("General Settings")
                .OnClick(x => x.Go<Admin.Settings.GeneralPage>());

        }
    }
}