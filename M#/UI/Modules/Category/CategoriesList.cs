using MSharp;

namespace Modules
{
    public class CategoriesList : ListModule<Domain.Category>
    {
        public CategoriesList()
        {
            HeaderText("Categories")
                .UseDatabasePaging(false)
                .Sortable()
                .ShowFooterRow()
                .ShowHeaderRow()
                .PagerPosition(PagerAt.Bottom);

            Search(GeneralSearch.AllFields).Label("Find");

            SearchButton("Search").OnClick(x => x.Reload());

            ImageColumn("Image Light").ImageUrl("@item.ImageLight");

            ImageColumn("Image Dark").ImageUrl("@item.ImageDark");
            
            ButtonColumn("c#:item.Name")
                .Style(ButtonStyle.Link)
                .HeaderText("Name")
                .OnClick(x => x.Go<Subject.SubjectsPage>()
                .Send("category", "item.ID"));

            Column(x => x.Group).HeaderTemplate("Group");


            Column(x => x.NumberOfSubjects).HeaderTemplate("Subjects");

            Column(x => x.NumberOfClips).HeaderTemplate("Clips");

            ButtonColumn("Edit")
                .HeaderText("Actions")
                .GridColumnCssClass("actions")
                .Icon(FA.Edit)
                .OnClick(x => x.PopUp<Category.EnterPage>()
                .Send("item", "item.ID").SendReturnUrl(false));

            ButtonColumn("Delete")
                .HeaderText("Actions")
                .GridColumnCssClass("actions")
                .ConfirmQuestion("Are you sure you want to delete this Category?")
                .CssClass("btn-danger")
                .Icon(FA.Remove)
                .OnClick(x =>
                {
                    x.DeleteItem();
                    x.RefreshPage();
                });

            Button("New Category")
                .Icon(FA.Plus)
                .OnClick(x => x.PopUp<Category.EnterPage>().SendReturnUrl(false));
        }
    }

}
