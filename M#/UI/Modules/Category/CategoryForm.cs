using MSharp;

namespace Modules
{
    public class CategoryForm : FormModule<Domain.Category>
    {
        public CategoryForm()
        {
            HeaderText("Category Details");                        
           
            Field(x => x.Name).Control(ControlType.Textbox).Mandatory();

            Field(x => x.Group).Control(ControlType.Textbox).SkipGroupControlWrapper();

            Field(x => x.ImageLight).Control(ControlType.FileUpload);

            Field(x => x.ImageDark).Control(ControlType.FileUpload);

            Button("Cancel").CausesValidation(false)
                .OnClick(x => x.CloseModal());

            Button("Save").IsDefault().Icon(FA.Check)
                .OnClick(x =>
                {
                    x.SaveInDatabase();
                    x.GentleMessage("Saved successfully.");
                    x.CloseModal(Refresh.Ajax);
                });
        }
    }
}
