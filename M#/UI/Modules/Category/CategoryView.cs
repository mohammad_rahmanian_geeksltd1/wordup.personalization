using MSharp;

namespace Modules
{
    public class CategoryView : ViewModule<Domain.Category>
    {
        public CategoryView()
        {
            HeaderText("@item.Name Details");

            Field(x => x.Name);

            Field(x => x.Group);

            Button("Back")
                .IsDefault()
                .Icon(FA.ChevronLeft)
                .OnClick(x => x.ReturnToPreviousPage());

            Button("Delete")
                .ConfirmQuestion("Are you sure you want to delete this category?")
                .CssClass("btn-danger")
                .Icon(FA.Remove)
                .OnClick(x =>
                {
                    x.DeleteItem();
                    x.GentleMessage("Deleted successfully.");
                    x.ReturnToPreviousPage();
                });
        }
    }
}
