using MSharp;

namespace Modules
{
    public class ClipForm : FormModule<Domain.Clip>
    {
        public ClipForm()
        {
            HeaderText("Clip Details");

            Field(x => x.Subject).Control(ControlType.DropdownList).Mandatory();

            Field(x => x.Url).Control(ControlType.Textbox).Mandatory();

            Button("Cancel").CausesValidation(false)
                .OnClick(x => x.CloseModal());

            Button("Save").IsDefault().Icon(FA.Check)
                .OnClick(x =>
                {
                    x.SaveInDatabase();
                    x.GentleMessage("Saved successfully.");
                    x.CloseModal(Refresh.Ajax);
                });
        }
    }
}
