using MSharp;

namespace Modules
{
    public class ClipsList : ListModule<Domain.Clip>
    {
        public ClipsList()
        {
            HeaderText("@Model.Subject.Category.Name > @Model.Subject.Name > Clips")
                .UseDatabasePaging(false)
                .Sortable()
                .PageSize(10)
                
                .ShowFooterRow()
                .ShowHeaderRow()
                .PagerPosition(PagerAt.Bottom);


            Search(x => x.Subject)
                    .AsDropDown();
           
            Search(GeneralSearch.AllFields).Label("Find");

            SearchButton("Search").OnClick(x => x.Reload());

            Column(x => x.Url);

            Column(x => x.VideoId);

            ButtonColumn("Edit")
                .HeaderText("Actions")
                .GridColumnCssClass("actions")
                .Icon(FA.Edit)
                .OnClick(x => x.PopUp<Clip.EnterPage>()
                .Send("item", "item.ID").SendReturnUrl(false));

            ButtonColumn("Delete")
                .HeaderText("Actions")
                .GridColumnCssClass("actions")
                .ConfirmQuestion("Are you sure you want to delete this Clip?")
                .CssClass("btn-danger")
                .Icon(FA.Remove)
                .OnClick(x =>
                {
                    x.DeleteItem();
                    x.RefreshPage();
                });

            Button("Add")
                .Icon(FA.Plus)
                .OnClick(x => x.PopUp<Clip.EnterPage>().SendReturnUrl(false));

            Button("Back")              
               .OnClick(x => x.Go<Subject.SubjectsPage>().SendReturnUrl(false));
        }
    }

}
