using MSharp;

namespace Modules
{
    public class SubjectForm : FormModule<Domain.Subject>
    {
        public SubjectForm()
        {
            HeaderText("Subject Details");

            Field(x => x.Category).Control(ControlType.DropdownList).Mandatory();
            Field(x => x.Name).Control(ControlType.Textbox).Mandatory();
            Field(x => x.Image).Control(ControlType.FileUpload);

            Button("Cancel").CausesValidation(false)
                .OnClick(x => x.CloseModal());

            Button("Save").IsDefault().Icon(FA.Check)
                .OnClick(x =>
                {
                    x.SaveInDatabase();
                    x.GentleMessage("Saved successfully.");
                    x.CloseModal(Refresh.Ajax);
                });
        }
    }
}
