using MSharp;

namespace Modules
{
    public class SubjectView : ViewModule<Domain.Subject>
    {
        public SubjectView()
        {
            HeaderText("@item.Name Details");

            Field(x => x.Category);
            Field(x => x.Name);
            Field(x => x.Image);

            Button("Back")
                .IsDefault()
                .Icon(FA.ChevronLeft)
                .OnClick(x => x.ReturnToPreviousPage());

            Button("Delete")
                .ConfirmQuestion("Are you sure you want to delete this Subject?")
                .CssClass("btn-danger")
                .Icon(FA.Remove)
                .OnClick(x =>
                {
                    x.DeleteItem();
                    x.GentleMessage("Deleted successfully.");
                    x.ReturnToPreviousPage();
                });
        }
    }
}
