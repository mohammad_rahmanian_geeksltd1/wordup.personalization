using MSharp;
using System;

namespace Modules
{
    public class SubjectsList : ListModule<Domain.Subject>
    {
        public SubjectsList()
        {
            HeaderText("Subjects")
                .UseDatabasePaging(false)
                .Sortable()
                .PageSize(50)
                .ShowFooterRow()
                .ShowHeaderRow()               
                .PagerPosition(PagerAt.Bottom);

           // SourceCriteria("CurrentCategory.ID !=null ? item.CategoryId == CurrentCategory.ID: item.CategoryId !=null");

            ViewModelProperty<Guid>("CategoryID");

            Search(x => x.Category)
                    .AsDropDown();
            Search(GeneralSearch.AllFields).Label("Find");

            SearchButton("Search").OnClick(x => x.Reload());

            ImageColumn("Image").ImageUrl("@item.Image");

            ButtonColumn("c#:item.Name")
                .Style(ButtonStyle.Link)
                .HeaderText("Name")
                .OnClick(x => x.Go<Clip.ClipsPage>()
                .Send("Subject", "item.ID"));

            Column(x => x.Category);
            Column(x => x.NumberOfClips).HeaderTemplate("Clips"); 

            ButtonColumn("Edit")
                .HeaderText("Actions")
                .GridColumnCssClass("actions")
                .Icon(FA.Edit)
                .OnClick(x => x.PopUp<Subject.EnterPage>()
                .Send("item", "item.ID").SendReturnUrl(false));

            ButtonColumn("Delete")
                .HeaderText("Actions")
                .GridColumnCssClass("actions")
                .ConfirmQuestion("Are you sure you want to delete this Subject?")
                .CssClass("btn-danger")
                .Icon(FA.Remove)
                .OnClick(x =>
                {
                    x.DeleteItem();
                    x.RefreshPage();
                });

            Button("New Subject")
                .Icon(FA.Plus)
                .OnClick(x => x.PopUp<Subject.EnterPage>().SendReturnUrl(false));

            
        }
    }

}
