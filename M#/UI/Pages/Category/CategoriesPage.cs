using MSharp;

namespace Category
{
    public class CategoriesPage : SubPage<CategoryPage>
    {
        public CategoriesPage()
        {
            Layout(Layouts.AdminDefault);

            Set(PageSettings.LeftMenu, "AdminSettingsMenu");

            Add<Modules.CategoriesList>();
        }
    }
}
