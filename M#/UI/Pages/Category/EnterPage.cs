using MSharp;

namespace Category
{
    public class EnterPage : SubPage<CategoriesPage>
    {
        public EnterPage()
        {
            Layout(Layouts.AdminDefaultModal);

            Add<Modules.CategoryForm>();
        }
    }

}
