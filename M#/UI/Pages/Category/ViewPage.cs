using MSharp;

namespace Category
{
    public class ViewPage : SubPage<CategoriesPage>
    {
        public ViewPage()
        {
            Layout(Layouts.AdminDefault);

            Set(PageSettings.LeftMenu, "AdminSettingsMenu");

            Add<Modules.CategoryView>();
        }
    }

}
