using MSharp;

public class CategoryPage : RootPage
{
    public CategoryPage()
    {
        Roles(AppRole.Admin);

        Add<Modules.MainMenu>();

        OnStart(x => x.Go<Category.CategoriesPage>().RunServerSide());
    }
}
