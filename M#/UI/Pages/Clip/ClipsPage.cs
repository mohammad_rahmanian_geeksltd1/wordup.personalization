using MSharp;

namespace Clip
{
    public class ClipsPage : SubPage<ClipPage>
    {
        public ClipsPage()
        {
            Layout(Layouts.AdminDefault);

            Set(PageSettings.LeftMenu, "AdminSettingsMenu");

            Add<Modules.ClipsList>();
        }
    }
}
