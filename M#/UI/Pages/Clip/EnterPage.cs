using MSharp;

namespace Clip
{
    public class EnterPage : SubPage<ClipsPage>
    {
        public EnterPage()
        {
            Layout(Layouts.AdminDefaultModal);

            Add<Modules.ClipForm>();
        }
    }

}
