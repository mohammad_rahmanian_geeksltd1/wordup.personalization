using MSharp;

namespace Clip
{
    public class ViewPage : SubPage<ClipsPage>
    {
        public ViewPage()
        {
            Layout(Layouts.AdminDefault);

            Set(PageSettings.LeftMenu, "AdminSettingsMenu");

            Add<Modules.ClipView>();
        }
    }

}
