using MSharp;

public class ClipPage : RootPage
{
    public ClipPage()
    {
        Roles(AppRole.Admin);

        Add<Modules.MainMenu>();

        OnStart(x => x.Go<Clip.ClipsPage>().RunServerSide());
    }
}
