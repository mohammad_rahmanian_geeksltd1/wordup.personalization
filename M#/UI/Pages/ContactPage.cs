using MSharp;

public class ContactPage : RootPage
{
    public ContactPage()
    {
        Roles(AppRole.Admin);

        Add<Modules.MainMenu>();

        OnStart(x => x.Go<Contact.ContactsPage>().RunServerSide());
    }
}
