using MSharp;

namespace Subject
{
    public class EnterPage : SubPage<SubjectsPage>
    {
        public EnterPage()
        {
            Layout(Layouts.AdminDefaultModal);

            Add<Modules.SubjectForm>();
        }
    }

}
