using MSharp;

namespace Subject
{
    public class SubjectsPage : SubPage<SubjectPage>
    {
        public SubjectsPage()
        {
            Layout(Layouts.AdminDefault);

            Set(PageSettings.LeftMenu, "AdminSettingsMenu");

            Add<Modules.SubjectsList>();
        }
    }
}
