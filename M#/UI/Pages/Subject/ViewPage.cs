using MSharp;

namespace Subject
{
    public class ViewPage : SubPage<SubjectsPage>
    {
        public ViewPage()
        {
            Layout(Layouts.AdminDefault);

            Set(PageSettings.LeftMenu, "AdminSettingsMenu");

            Add<Modules.SubjectView>();
        }
    }

}
