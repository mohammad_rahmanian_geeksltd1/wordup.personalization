using MSharp;

public class SubjectPage : RootPage
{
    public SubjectPage()
    {
        Roles(AppRole.Admin);

        Add<Modules.MainMenu>();

        OnStart(x => x.Go<Subject.SubjectsPage>().RunServerSide());
    }
}
