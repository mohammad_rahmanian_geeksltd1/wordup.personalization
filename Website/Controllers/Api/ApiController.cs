using Domain;
using Microsoft.AspNetCore.Mvc;
using Olive;
using Olive.Mvc;
using Olive.Security;
using Olive.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Website.ViewModel;

namespace Controllers
{
    public class ApiController : BaseController
    {
        [Route("api/categories")]
        public async Task<ActionResult> Categories()
        {
            var query = Database.Of<Category>();
            var result = await query.GetList();

            return Ok(result);
        }

        [Route("api/subjects/{Id:int?}")]
        public async Task<ActionResult> Subjects(int Id)
        {
            var query = Database.Of<Subject>();
            var result = await query.Where(x => x.CategoryId == Id).GetList();
            result.Do(x => x.Category = null);
            return Ok(result);
        }

        [Route("api/clips/{Id:int?}")]
        public async Task<ActionResult> Clips(int Id)
        {
            var query = Database.Of<Clip>();
            var result = await query.Where(x => x.SubjectId == Id).GetList();
            result.Do(x => x.Subject = null);
            return Ok(result);
        }

        [Route("api/all")]
        public async Task<ActionResult> All()
        {
            var Categories = new List<CategoryVM>();

            var query1 = Database.Of<Category>();
            var _categories = await query1.GetList().ToList();          

            foreach (var category in _categories)
            {
                var categoryvm = new CategoryVM();
                categoryvm.Id = category.ID;
                categoryvm.Name = category.Name;
                categoryvm.Group = category.Group;
                categoryvm.ImageDark = category.ImageDark.Url();
                categoryvm.ImageLight = category.ImageLight.Url();
                var query2 = Database.Of<Subject>();
                var _subjects = await query2.Where(x=>x.CategoryId == category.ID).GetList().ToList();
                
                foreach (var subject in _subjects)
                {
                    var subjectvm = new SubjectVM();
                    subjectvm.Id = subject.ID;
                    subjectvm.Name = subject.Name;
                    subjectvm.CategoryId= category.ID;
                    subjectvm.Group = category.Group;
                    subjectvm.Image = subject.Image.Url();
                    categoryvm.Subjects.Add(subjectvm);
                }
                Categories.Add(categoryvm);
            }
 
            return Ok(Categories);
        }
      
    }

}
