﻿namespace Website.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CategoryVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageLight { get; set; }
        public string ImageDark { get; set; }
        public string Group { get; set; }
        public ICollection<SubjectVM> Subjects { get; set; }= new HashSet<SubjectVM>();

    }
}
