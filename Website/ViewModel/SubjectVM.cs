﻿namespace Website.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.Json.Serialization;
    using System.Threading.Tasks;

    public class SubjectVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public CategoryVM Category { get; set; }
        public int CategoryId { get; set; }
        public string Group { get; set; }
        public string Image { get; set; }

    }
}
